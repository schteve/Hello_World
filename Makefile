# MAKEFILE

CFLAGS = -Wall 

CXX = g++

%: src/%.cc
	$(CXX) $(CFLAGS) $< -o bin/$@

.PHONY: clean
clean:
	rm *.o
